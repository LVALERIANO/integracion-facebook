from flask import Flask, request
app = Flask(__name__)

VERIFY_TOKEN= 'Conexionexitosa'

@app.route('/')
def index():

    if(request.args.get('hub.verify_token', '') == 'VERIFY_TOKEN'):
        print('verified')
        # Devuelve una solicitud de verificacion de token
        return request.args.get('hub.challenge', '')
    else:
        print("Error token")
        return "Error, validacion token"

if __name__ == '__main__':
    app.run(debug=True)
